const router = require('express').Router();
const controller = require('../controllers/dashboard/index');
const dashboard = require('./dashboard');
const auth = require('./auth');

// routes
router.use('/auth', auth);
router.get('/', controller.home);
router.use('/dashboard', dashboard);

// router.use(controller.notFound);
// router.get(controller.exception);

module.exports = router;

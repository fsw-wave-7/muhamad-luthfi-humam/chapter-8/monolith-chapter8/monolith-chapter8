const router = require('express').Router();
const auth = require('../controllers/auth/index');
const layoutName = (req, res, next) => {
  res.locals.layout = `layouts/${layoutName}`;
  next();
};

// router.use(layoutName(auth));
router.get('/login', auth.login);
router.post('/login', auth.api.login);
router.get('/register', auth.register);
router.post('/register', auth.api.register);

module.exports = router;

const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const User = require('../models/user');

passport.use(
  new localStrategy((username, password, done) => {
    User.findOne({ username }).then((user) => {
      if (!user) return done(null, false);
      if (!user.verifyPassword(password)) return done(null, false);
      return done(null, user);
    });
  })
);

passport.serializeUser((user, done) => done(null, user.id));
passport.deserializeUser(async (id, none) => done(null, await User.findByPk(id)));
module.exports = passport;

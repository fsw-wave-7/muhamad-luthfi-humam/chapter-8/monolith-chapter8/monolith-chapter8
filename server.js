const express = require('express');
const app = express();
const { PORT = 8000 } = process.env;
const expressLayout = require('express-ejs-layouts');
const morgan = require('morgan')
// Middleware, pasang sebelum routing
const setDefault = (req, res, next) => {
  res.locals.contentName = 'Default';
  // Tambahkan varibel lain jika diperlukan
  next();
};
// view engine
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.use(expressLayout);
app.set('layout', 'layouts/default')
app.use(morgan('dev'))
// parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// taruh code ini setelah setup views engine dan parser
const router = require('./router')
app.use(router)

app.listen(PORT, () => {
  console.log(`Listening on http://localhost:${PORT}`);
});

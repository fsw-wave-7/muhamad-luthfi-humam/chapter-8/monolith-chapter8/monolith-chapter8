const moment = require('moment');
const post = [
  { id: 1, title: 'Lorem Ipsum', body: 'Dolor sit amet', createdAt: Date() },
  { id: 2, title: 'Lorem Ipsum', body: 'Dolor sit amet', createdAt: Date() },
];
module.exports = {
  index: (req, res) => {
    const locals = {
      data: {
        post: post.map((i) => {
          i.fromNow = moment(i.createdAt).fromNow();
          return i;
        }),
      },
      contentName: 'Post',
      layout: 'layouts/dashboard',
    };
    res.render('pages/dashboard/post', locals);
  },
};

const { Post, Visitor } = require('../../models');

module.exports = {
  home:async (req, res) => {
    const locals = {
      data: [
        {
          Post:  "coba",
          Visitor:  5,
          // Reader: await Post.sum('title'),
        },
      ],
      contentName: 'Statistic',
      layout: 'layouts/dashboard',
    };
    res.render('pages/dashboard/home', locals);
  },
  post: require('./post'),
};

const passport = require('../../lib/passport');
const bcrypt = require('bcrypt');
const { User } = require('../../models');

module.exports = {
  register: (req, res) => {
    User.create({
      username: req.body.username,
      password: bcrypt.hashSync(req.body.password, 10),
    }).then(() => {
      res.redirect('/auth/login');
    });
  },
  login: (req, res) => {
    User.findOne({
      username: req.body.username,
      password: bcrypt.hashSync(req.body.password, 10),
    }).then(() => {
      res.redirect('/dashboard');
    });
  },
};

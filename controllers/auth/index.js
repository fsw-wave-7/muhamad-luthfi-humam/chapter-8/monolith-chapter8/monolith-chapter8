module.exports = {
  login: (req, res) =>
    res.render('pages/authentication/login', {
      contentName: 'Login',
      layout: 'layouts/dashboard',
    }),
  register: (req, res) => res.render('pages/authentication/register', {
    contentName: 'Register',
    layout: 'layouts/dashboard',
  }), 
  api: require('./api'),
};
